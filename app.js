promise1 = fetch("https://ajax.test-danit.com/api/swapi/films")
  .then((response) => {
    return response.json();
  })
  .then((films) => {
    films.forEach((film) => {
      displayFilmInfo(film);

      promise2 = film.characters.forEach((character) => {
        fetch(character)
          .then((response) => {
            return response.json();
          })
          .then((data) => {
            displayCharacter(data, film.episodeId);
          });
      });
    });
  });

function displayCharacter(character, episodeId) {
  console.log(character.name);
  document
    .getElementById(episodeId)
    .insertAdjacentHTML("afterend", `<div>:Character: ${character.name}</div>`);
}

function displayFilmInfo(film) {
  document.body.insertAdjacentHTML(
    "afterbegin",
    `<div ><b> Episod</b>: ${film.episodeId}</div>
            <div id="${film.episodeId}">Name: ${film.name}</div>
            <div>Description: ${film.openingCrawl}</div>
           `
  );
}
